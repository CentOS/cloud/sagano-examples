# autonomous-podman-hello

This example has three ingredients:

- A trivial [podman-systemd unit](usr/share/containers/sytsemd/hello.container) that runs
  an instance of the Caddy webserver (just to have a service to talk to)
- [Automatic updates enabled by default](usr/lib/systemd/system/autoupdate-host.timer)
- Disabled OpenSSH completely (see [Containerfile](Containerfile))

The idea here is that the system should be managed in an "immutable infrastructure"
model; we don't want a mechanism for "state drift" as is easy to have happen
with unconstrained SSH logins.

Instead, we can change the state of the system by pushing a new container to
a registry - when it changes, the host will automatically fetch it and reboot.


## Demo image

`registry.gitlab.com/centos/cloud/sagano-examples/autonomous-podman-hello:latest`
